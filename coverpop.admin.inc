<?php
/**
 * Page callback: Cover Pop settings.
 *
 * @see coverpop_menu()
 */

/**
 * Implements hook_form().
 */
function coverpop_form($form, &$form_state) {

  /* Fieldset for CoverPop Settings */
  $form['coverpop_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('CoverPop settings'),
    '#weight' => 1,
    '#collapsible' => TRUE,
  );
  $form['coverpop_settings']['coverpop_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay (ms)'),
    '#default_value' => variable_get('coverpop_delay', COVERPOP_DELAY),
    '#description' => t('Delay before the PopUp appears. Set 0 to appear instantly.'),
    '#required' => FALSE,
  );
  $form['coverpop_settings']['coverpop_hideafter'] = array(
    '#type' => 'textfield',
    '#title' => t('Autohide (ms)'),
    '#default_value' => variable_get('coverpop_hideafter', COVERPOP_HIDEAFTER),
    '#description' => t('The amount of time in milliseconds before the PopUp hides. Set 0 to never hide.'),
    '#required' => FALSE,
  );
  $form['coverpop_settings']['coverpop_escape'] = array(
    '#type' => 'textfield',
    '#title' => t('Close with escape'),
    '#default_value' => variable_get('coverpop_escape', COVERPOP_ESCAPE),
    '#description' => t('If the escape key can be used to close the CoverPop Up.'),
    '#required' => FALSE,
  );

  /* Fieldset for CoverPop Selector Settings */
  $form['selector_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Selector settings'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['selector_settings']['coverpop_close'] = array(
    '#type' => 'textfield',
    '#title' => t('Close without default action'),
    '#default_value' => variable_get('coverpop_close', COVERPOP_CLOSE),
    '#description' => t('Provide a jQuery selector for closing item. Default action will NOT be executed. Example: CoverPop-go'),
    '#required' => FALSE,
  );
  $form['selector_settings']['coverpop_close_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Close With default action'),
    '#default_value' => variable_get('coverpop_close_default', COVERPOP_CLOSE_DEFAULT),
    '#description' => t('Provide a jQuery selector for closing item. Default action will be executed. Example: CoverPop-close-go'),
    '#required' => FALSE,
  );
  $form['selector_settings']['coverpop_close_noshow'] = array(
    '#type' => 'textfield',
    '#title' => t('Close with Max Cookie time'),
    '#default_value' => variable_get('coverpop_close_noshow', COVERPOP_CLOSE_NOSHOW),
    '#description' => t('Provide a jQuery selector for closing item. Sets the Max Cookie time. Example: CoverPop-close-no-show'),
    '#required' => FALSE,
  );

  /* Fieldset for CoverPop Cookie Settings */
  $form['cookie_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cookie settings'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['cookie_settings']['coverpop_expires'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie minimal (days)'),
    '#default_value' => variable_get('coverpop_expires', COVERPOP_EXPIRES),
    '#description' => t('Amount of days before next pop-up.'),
    '#required' => FALSE,
  );
  $form['cookie_settings']['coverpop_expiresnoshow'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie maximal (days)'),
    '#default_value' => variable_get('coverpop_expiresnoshow', COVERPOP_EXPIRENOSHOW),
    '#description' => t('Amount of days before next pop-up if user choose not to show-up.'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_validate().
 */
function coverpop_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['coverpop_expires'])) {
    form_set_error('coverpop_expires', t('Cookie minimal must be Numeric'));
  }
  if (!is_numeric($form_state['values']['coverpop_expiresnoshow'])) {
    form_set_error('coverpop_expiresnoshow', t('Cookie maximal must be Numeric'));
  }
  if (!empty($form_state['values']['coverpop_delay']) && !is_numeric($form_state['values']['coverpop_delay'])) {
    form_set_error('coverpop_delay', t('Delay must be Numeric'));
  }
  if (($form_state['values']['coverpop_delay']) < 0) {
    form_set_error('coverpop_delay', t('Delay must be equal or greater then 0.'));
  }
  if (!is_numeric($form_state['values']['coverpop_hideafter'])) {
    form_set_error('coverpop_hideafter', t('Autohide must be Numeric'));
  }
}
