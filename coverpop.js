/**
 * Using Drupal variables as options for CoverPop JS.
 */
(function ($) {

  Drupal.behaviors.CoverPop = {
    attach: function (context, settings) {

      var parser = document.createElement('a');
      parser.href = window.location.href;
      var path = parser.pathname;
      var delay = settings.coverpop.delay || 0;
      var expires = settings.coverpop.expires || 0;
      var expiresLong = settings.coverpop.expiresNoShow || 0;       // Make sure the zero value return integer 0, instead of NaN.
      var c = new CoverPop({
        expires: expires,						                    // duration (in days) before it pops up again
        expiresLong: expiresLong,                                   // duration if users choose not to show again
        closeClassNoDefault: settings.coverpop.closeClassNoDefault, // close if someone clicks an element with this class and prevent default action
        closeClassDefault: settings.coverpop.closeClassDefault,     // close if someone clicks an element with this class and continue default action
        closeClassNoShow: settings.coverpop.closeClassNoShow,       // close and stops popup from showing again for as long as expireNoShow
        cookieName: path,                                           // to change the plugin cookie name
        onPopUpOpen: null,                                          // on popup open callback function
        onPopUpClose: null,                                         // on popup close callback function
        forceHash: 'splash',                                        // hash to append to url to force display of popup (e.g. http://yourdomain.com/#splash)
        delayHash: 'go',                                            // hash to append to url to delay popup for 1 day (e.g. http://yourdomain.com/#go)
        closeOnEscape: settings.coverpop.closeOnEscape,             // close if the user clicks escape
        delay: delay,                                               // set an optional delay (in milliseconds) before showing the popup
        hideAfter: settings.coverpop.hideAfter                      // set an optional time (in milliseconds) to autohide
      });
      c.init();
    }
  }

}(jQuery));
